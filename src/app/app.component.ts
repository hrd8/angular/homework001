import {Component, DoCheck} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements DoCheck{
  ngDoCheck(): void {
      this.isRedText = this.inputText === "red" || this.inputText === "RED";
  }
  isRedText:boolean = false;
  inputText:string = "";
  title = 'practice002';
  isLightOn:boolean = false;
  imageUrl:string ='../assets/off.png';
  toggleLight = ():void=>{
    if(this.isLightOn){
      this.imageUrl='../assets/on.png';
    }else{
      this.imageUrl='../assets/off.png';
    }
    this.isLightOn=!this.isLightOn;
  }

}
